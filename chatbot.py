import spacy
import sqlite3
from datetime import datetime

# python -m spacy download en_core_web_sm
nlp = spacy.load('en_core_web_sm')

def get_dependencies(sentence):
    """
    parse with dependency parser from spacy
    """
    sentence = nlp(sentence)
    for token in sentence:
        if token.dep_ == 'advmod':
            q = token
        elif token.dep_ == 'nsubj':
            subj = token
        elif token.dep_ == 'pobj':
            obj = token
        elif token.dep_ == 'ROOT':
            pred = token
    return (q,pred,subj,obj)

def get_games(team1,team2,database):
    """
    get games from database where team participated
    """
    connection = sqlite3.connect(database)
    cursor = connection.cursor()
    sql_command = """
        SELECT Date,HomeTeam,AwayTeam,FTHG,FTAG FROM football_data
        WHERE (HomeTeam = '{team1}' OR AwayTeam = '{team1}') AND (HomeTeam = '{team2}' OR AwayTeam = '{team2}');
    """.format(team1=team1,team2=team2)
    cursor.execute(sql_command)
    result = cursor.fetchall()
    connection.close()
    return result

def normalize_result(result,team):
    """
    normalize 'home' and 'away' scores to absolute 'for' and 'against' scores
    """
    date,home,away,score_home,score_away = result
    if home != team:
        # swap home and away results
        home,away = away,home
        score_home,score_away = score_away,score_home
    result = {
        'date':date,
        'for':home,
        'against':away,
        'score_for':score_home,
        'score_against':score_away
    }
    return (result)

def answer(sentence, database='data/database.sqlite'):
    """
    generate answer string which contains the results for the last game between the two teams asked
    """

    _,_,subj,obj = get_dependencies(sentence)

    results_tuples = get_games(subj.text,obj.text,database)

    normalized_results = [normalize_result(r,subj.text) for r in results_tuples]

    if len(normalized_results) == 0:
        return ("I couldn't find any games.")
    last_game = normalized_results[0]

    # more readable format for date
    date = datetime.fromisoformat(last_game['date'])
    dateformat = '%A, %B %d, %Y'
    date = datetime.strftime(date, dateformat)

    if last_game['score_for'] > last_game['score_against']:
        answer = '{team_for} won against {team_against} by {score_for}:{score_against} on {date}.'.format(team_for = subj.text, team_against = obj.text, score_for = last_game['score_for'], score_against = last_game['score_against'],date=date)
    elif last_game['score_for'] < last_game['score_against']:
        answer = '{team_for} lost against {team_against} by {score_for}:{score_against} on {date}.'.format(team_for = subj.text, team_against = obj.text, score_for = last_game['score_for'], score_against = last_game['score_against'],date=date)
    else:
        answer = '{team_for} and {team_against} played draw on {date}. The final score was {score_for}:{score_against}.'.format(team_for = subj.text, team_against = obj.text, score_for = last_game['score_for'], score_against = last_game['score_against'],date=date)
    return (answer)
